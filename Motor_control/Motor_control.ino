#include <FastPID.h>
#include <Scheduler.h>
#include <CircularBuffer.h>

const int PWM_OUTPUT_PIN = 9;
const int PHOTODIODE_INPUT_PIN = A3;
const int BUFF_SIZE = pow(4,6);
const int blades_num=16;
const int sample_num=2*pow(4,5); //number of samples for frequency estimation
int pt_avg=0; // temp variable to hold the average photodiode
int avg_num=30; // number of points to window average
CircularBuffer<int, BUFF_SIZE> photodiode_val;
CircularBuffer<int, BUFF_SIZE> photodiode_avg_val;
CircularBuffer<float, BUFF_SIZE> frequency;
CircularBuffer<int, BUFF_SIZE> check_delete_me;

int count =0;
int reading=0;  // last photodiode read
unsigned int thsld=230; // thershold for blade counting 

//unsigned int x=1; // jump in sense loop


byte dc_motor_control;
byte dc_motor_control_fixed = 85;

const unsigned long SIGNAL_PERIOD = 8000;
const unsigned long IO_PERIOD = 50;
unsigned long lastReadSig = 0;
unsigned long lastReadIO = 0;

float time_blade = (float)blades_num*IO_PERIOD*sample_num*pow(10,-6);
boolean pid_on = false; // true - PID on, false - PID off



//PIDs
float Kp=15;
float Ki=0;
float Hz = pow(10,6)/SIGNAL_PERIOD;
int16_t pidstep;
int16_t pid_feedbak;
int16_t setpoint = 155;

FastPID myPID(Kp, Ki, 0, Hz, 7, false);


void setup() {
  pinMode(PWM_OUTPUT_PIN, OUTPUT);
  pinMode(PHOTODIODE_INPUT_PIN, INPUT);
  
  dc_motor_control = dc_motor_control_fixed;
  analogWrite(PWM_OUTPUT_PIN, dc_motor_control);
  myPID.setOutputRange(65, 250);
  
  for (int i=0; i<BUFF_SIZE; i++) {
    photodiode_val.push(0); 
    frequency.push(0);
    photodiode_avg_val.push(0);
//    control_signal[i] = 0; 
  }
  
  
  Serial.begin(9600); // opens serial port, sets data rate to 9600 bps
  Serial.print("time blade: ");Serial.println(time_blade);
  Serial.print("start ind: ");Serial.println(BUFF_SIZE-sample_num+3);
  Serial.print("end ind: ");Serial.println(BUFF_SIZE-2);


  Scheduler.startLoop(io_loop);
  Scheduler.startLoop(control_sense_loop);
}

void loop() {
  if (Serial.available() > 0) {
    //Serial.print("serial handle");
    handle_serial();
  }
  delay(10);
}

void io_loop() {
    unsigned long d = micros() - lastReadIO;
    if (d >= IO_PERIOD) {
      lastReadIO += d;
      // Read photo-diode data
      reading = analogRead(PHOTODIODE_INPUT_PIN);
      photodiode_val.push(reading);
      
      // insert to average vector the average of last 5 measurements
//      pt_avg=0;
//      for (int i=0;i<avg_num;i++){
//        pt_avg = pt_avg+photodiode_val[BUFF_SIZE-i-1];
//      }
//      photodiode_avg_val.push(pt_avg/avg_num);
      photodiode_avg_val.push(photodiode_avg_val.last()+((float)(photodiode_val[BUFF_SIZE-1]-photodiode_val[BUFF_SIZE-avg_num])/(float)avg_num));
      // 3. Perform any per-sample calculation for frequency estimation
      // 4. Write control singal to PWM
    }
    yield();
}

void control_sense_loop() {
    unsigned long d = micros() - lastReadSig;
    if (d >= SIGNAL_PERIOD) {
      lastReadSig += d;
      count=0;
      //Estimate the fuency based on recent input measurements
    for (int i=(BUFF_SIZE-sample_num+3); i<(BUFF_SIZE-2); i++) {
      if ((photodiode_avg_val[i]>=thsld)&&(photodiode_avg_val[i+1]>=thsld)&&(photodiode_avg_val[i+2]>=thsld)&&(photodiode_avg_val[i-1]<=thsld)&&(photodiode_avg_val[i-2]<=thsld)) {
        count++; // count a blade
        i=i+4; // go to next colsest step
      }
    }
      
    //Update control_signal and frequency buffers
    frequency.push(count/time_blade);
    //Serial.println(count);
      
    if (pid_on) {
      //Update control signal values (if in closed loop mode)
      pid_feedbak=(int16_t)(count/time_blade);
      pidstep = myPID.step(setpoint, pid_feedbak);
      dc_motor_control = (byte)pidstep;
      analogWrite(PWM_OUTPUT_PIN, dc_motor_control);
    }
  }
  yield();
}


void handle_serial() {
  delay(5); 
  int Instruction_int;
  Instruction_int = Serial.parseInt();
  flush_serial();
  
  if (Instruction_int == 0) {
    // return photo diode vector
    for (int i=1; i<BUFF_SIZE; i++) {
      Serial.print(photodiode_val[i]);
      Serial.print(' ');
    }
  Serial.print("\n");  
  }
    
  if (Instruction_int == 1) {
    // return frequency vector
    for (int i=1; i<BUFF_SIZE; i++) {
      Serial.print(frequency[i]);
      Serial.print(' ');
    }
    Serial.print("\n");  
  }

  if (Instruction_int == 3) {
  // step response using io loop
  // get frequency data
  
  // start slow
  analogWrite(PWM_OUTPUT_PIN, 180);
  // wait 1/4 of frequency vector
  delay(SIGNAL_PERIOD*BUFF_SIZE/4000);

  // go fast fan 
  analogWrite(PWM_OUTPUT_PIN, 250);
  // wait the 3/4 of the frequency vector
  delay(3*SIGNAL_PERIOD*BUFF_SIZE/4000);

  // print frequency vector
  for (int i=1; i<BUFF_SIZE; i++) {
    Serial.print(frequency[i]);
    Serial.print(' ');
  }
  Serial.print("\n");
  
  // back normal
  analogWrite(PWM_OUTPUT_PIN, dc_motor_control_fixed);

  
  
  }


  if (Instruction_int == 5) {
  // start pid
  pid_on=true;
  Serial.print("set_point: ");
  Serial.println(setpoint);
  Serial.print("pid feedback: ");
  Serial.println(pid_feedbak);
  Serial.print("dc motor control: ");
  Serial.println(dc_motor_control);
  }

  if (Instruction_int == 6) {
  // stop pid
  pid_on=false;
  analogWrite(PWM_OUTPUT_PIN, dc_motor_control_fixed);
  }

  if (Instruction_int == 7) {
  // step response using io loop
  // get photodiode data
  
  // start slow
  analogWrite(PWM_OUTPUT_PIN, 180);
  for (int i=0;i<BUFF_SIZE;i++){
    reading = analogRead(PHOTODIODE_INPUT_PIN);
    photodiode_val.push(reading);
    delayMicroseconds(IO_PERIOD);
  }

  // print photodiode vector
  for (int i=1; i<BUFF_SIZE; i++) {
    Serial.print(photodiode_val[i]);
    Serial.print(' ');
  }
  Serial.print("\n");

  // go fast fan 
  analogWrite(PWM_OUTPUT_PIN, 250);
  // wait 1 times the photodiode vector
  for (int i=0;i<BUFF_SIZE;i++){
    reading = analogRead(PHOTODIODE_INPUT_PIN);
    photodiode_val.push(reading);
    delayMicroseconds(IO_PERIOD);
  }

  // print photodiode vector
  for (int i=1; i<BUFF_SIZE; i++) {
    Serial.print(photodiode_val[i]);
    Serial.print(' ');
  }
  Serial.print("\n");

  // back normal
  analogWrite(PWM_OUTPUT_PIN, dc_motor_control_fixed);
  
  }

  if (Instruction_int == 8) {
  // show photo diode val each second for 15 secnonds
  for (int i=0;i<15;i++) {
    Serial.println(analogRead(PHOTODIODE_INPUT_PIN));
    delay(1000);
  }
  }
  

   if ((Instruction_int > 10)&&(Instruction_int < 80)) {
  // set pid frequency to what we get
  pid_on=true;
  setpoint = (int16_t)Instruction_int; 
  Serial.print("set_point: ");
  Serial.println(setpoint);
  Serial.print("pid feedback: ");
  Serial.println(pid_feedbak);
  Serial.print("dc motor control: ");
  Serial.println(dc_motor_control);
  }

  if ((Instruction_int > 1000)&&(Instruction_int < 2000)) {
  // set PWM maunaly
  // where 1xxx set the PWM to be xxx
  dc_motor_control = Instruction_int % 1000;
  analogWrite(PWM_OUTPUT_PIN, dc_motor_control);
  }

  if ((Instruction_int > 2000)&&(Instruction_int < 3000)) {
  // set thsld value
  // where 1xxx set the thershold to be xxx
  thsld = Instruction_int % 1000;
  }
}


void flush_serial() {
  Serial.end();
  delay(100);
  Serial.begin(9600);
}
